import React, { Component } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { createSwitchNavigator, createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import Screen1 from "./screens/Screen1";
import Screen2 from "./screens/Screen2";
import Screen3 from "./screens/Screen3";
import Red from "./screens/Red";
import Green from "./screens/Green";
import Blue from "./screens/Blue";
import Home from "./screens/Home";
import Login from './screens/Login'
import Signup from './screens/Signup'

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
const Screen1Navigator = createStackNavigator({
  Screen1: Screen1,
  Red: Red,
  Blue: Blue,
  Green: Green
},
  {
    defaultNavigationOptions: {
      header: null
    }
  });
const Screen2Navigator = createStackNavigator({
  Screen2: Screen2,
  Red: Red,
  Blue: Blue,
  Green: Green
},
  {
    defaultNavigationOptions: {
      header: null
    }
  });
const Screen3Navigator = createStackNavigator({
  Screen3: Screen3,
  Red: Red,
  Blue: Blue,
  Green: Green
},
  {
    defaultNavigationOptions: {
      header: null
    }
  });
const AppStackNavigator = createDrawerNavigator(
  {
    Home: Home,
    Screen1: Screen1Navigator,
    Screen2: Screen2Navigator,
    Screen3: Screen3Navigator
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);
const AuthNavigator = createStackNavigator(
  {
    Login: Login,
    Signup: Signup
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
)
const AuthSwitchNavigator = createSwitchNavigator(
    {
      App: AppStackNavigator,
      Auth: AuthNavigator
},{
  initialRouteName: "Auth",
  backBehavior: "none"
})

const AppContainer = createAppContainer(
  AuthSwitchNavigator,
);
//createAppContainer(AppStackNavigator)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
