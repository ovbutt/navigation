import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class Blue extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text> Blue Screen </Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});