import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'

export default class Login extends Component {
    constructor(){
        super()
        this.state={login: false}
    }
  render() {
    return (
      <View style={styles.container}>
        <Text onPress={()=>this.props.navigation.navigate(this.state.login ? 'App' : 'Signup')}> Login Screen </Text>
        <Button title='Home Screen'onPress={()=>this.props.navigation.navigate('App')}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    }
  });
  