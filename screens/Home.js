import React, { Component } from "react";
import { Text, View, Button, StyleSheet } from "react-native";

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Screen 1"
          onPress={() => this.props.navigation.navigate("Screen1")}
        />
        <Button
          title="Screen 2"
          onPress={() => this.props.navigation.navigate("Screen2")}
        />
        <Button
          title="Screen 3"
          onPress={() => this.props.navigation.navigate("Screen3")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
