import React, { Component } from 'react'
import { Text, View, StyleSheet, Button } from 'react-native'

export default class Screen1 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Red"
          onPress={() => this.props.navigation.navigate("Red")}
        />
        <Button
          title="Green"
          onPress={() => this.props.navigation.navigate("Green")}
        />
        <Button
          title="Blue"
          onPress={() => this.props.navigation.navigate("Blue")}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});